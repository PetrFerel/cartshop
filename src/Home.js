import React from "react";
import Itemcart from "./Itemcart";
import data from "./data";
const Home = () => {
  return (
    <>
      <h1 className="text-center mt-3 h-100 shadow">All Items</h1>
      <section className="ry-4 container">
        <div className="row justify-content-center m-1 p-3">
          {data.productData.map((item, index) => {
            return (
              <Itemcart
                img={item.img}
                title={item.title}
                desc={item.price}
                item={item}
                key={index}
              />
            );
          })}
        </div>
      </section>
    </>
  );
};

export default Home;
