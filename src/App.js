import "./App.css";
import Header from "./Header";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Home from "./Home";
import Card from "./Card";
import { CartProvider } from "react-use-cart";
const headerdata = {
  siteName: "Food Shop",
};
function App() {
  return (
    <>
      <CartProvider>
        <Header data={headerdata} />
        <Home />
        <Card />
      </CartProvider>
    </>
  );
}

export default App;
