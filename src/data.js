import img1 from "./img/img-6.6.jpg";
import img2 from "./img/img-7.8.jpg";
import img3 from "./img/image-3.3webp.webp";
import img4 from "./img/img-5.5.jpg";

const data = {
  productData: [
    {
      id: 1,
      img: img1,
      title: "Grapes",
      desc: "",
      price: 6,
    },
    {
      id: 2,
      img: img2,
      title: "Mandarins",
      desc: "",
      price: 12,
    },
    {
      id: 3,
      img: img3,
      title: "Oranges",
      desc: "",
      price: 3,
    },
    {
      id: 4,
      img: img4,
      title: "Bananas",
      desc: "",
      price: 1.4,
    },
    // {
    //   id: 5,
    //   img: img1,
    //   title: "",
    //   desc: "",
    //   price: 46,
    // },
    // {
    //   id: 6,
    //   img: img2,
    //   title: "",
    //   desc: "",
    //   price: 46,
    // },
    // {
    //   id: 7,
    //   img: img3,
    //   title: "",
    //   desc: "",
    //   price: 46,
    // },
    // {
    //   id: 8,
    //   img: img2,
    //   title: "",
    //   desc: "",
    //   price: 46,
    // },
  ],
};
export default data;
