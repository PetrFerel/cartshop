import React from "react";

export default function Header(props) {
  return (
    <header className="header-site m-1">
      <h1 className="header-text">{props.data.siteName}</h1>
      <Nav />
    </header>
  );
}
function Nav() {
  return (
    <nav>
      <ul>
        <li>One</li>
        <li>Two</li>
        <li>Three</li>
      </ul>
    </nav>
  );
}
